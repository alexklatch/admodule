import sys
import os
from xml.dom import minidom
from distutils.dir_util import copy_tree
from distutils.dir_util import remove_tree
from distutils.dir_util import mkpath
from shutil import copyfile
from pathlib import Path

import time


def find_start_activity_name(path, folder):
    xmldoc = minidom.parse(path)

    package_name = ""
    package = xmldoc.getElementsByTagName("manifest")
    try:
        package_name = package[0].attributes["package"].value

    except Exception:
        print("package name not found")

    # package_name = package.attributes["package"].value

    itemlist = xmldoc.getElementsByTagName("activity")

    for item in itemlist:

        print(item.attributes["android:name"].value)
        categories_list = item.getElementsByTagName("category")
        print(categories_list)
        for category in categories_list:
            if "android.intent.category.LAUNCHER" in category.attributes["android:name"].value:

                name = item.attributes["android:name"].value
                if str(name).startswith(".") and len(package_name) > 0:
                    name = "{}{}".format(package_name, name)

                fn = os.path.basename(str(name).replace(".", "/"))
                print("fn=" + fn)

                for filename in Path(folder).rglob(fn + ".smali"):
                    fn = filename
                    break

                return str(fn)


def remove_clear_traffic_constrains(path):
    xmldoc = minidom.parse(path)
    itemlist = xmldoc.getElementsByTagName("application")

    for item in itemlist:
        if item.hasAttribute("android:networkSecurityConfig"):
            item.removeAttribute("android:networkSecurityConfig")

        if not item.hasAttribute("android:usesCleartextTraffic"):
            item.setAttribute("android:usesCleartextTraffic", "true")

    f = open(path, "w")
    xmldoc.writexml(f)
    f.close()


if __name__ == "__main__":

    print(sys.argv)

    script_name = sys.argv[0]
    inject_url = sys.argv[1]
    path_to_apk = sys.argv[2]
    result_dest = None
    package_name = None

    if len(sys.argv) > 3:
        result_dest = sys.argv[3]

    isFindStart = False
    index = 0

    # get folder name to sources files
    folder_source_apk = os.path.splitext(path_to_apk)[0]
    smali_folder = folder_source_apk + "/smali/"
    dist_folder = folder_source_apk + "/dist/"
    mkpath("tmp")

    # get file name from path
    apk_file_name = os.path.basename(path_to_apk)
    print("file name is " + apk_file_name)

    # start apktool
    os.system("java -jar apktool.jar -f d --frame-path {}  --use-aapt2 {}".format("tmp", path_to_apk))

    # copy inject files to sources
    copy_tree("code/", smali_folder)

    # remove clear traffic constrains
    remove_clear_traffic_constrains(folder_source_apk + "/AndroidManifest.xml")

    # inject code
    # analyze AndroidManifest.xml to find name of start Activity

    start_activity_path = find_start_activity_name(folder_source_apk + "/AndroidManifest.xml", folder_source_apk)

    print("start activity path = " + start_activity_path)

    # make smali path
    file_name = start_activity_path

    # open main file
    f = open(file_name, "r")
    # open inject file
    f1 = open("main_code.smali", "r")
    # print(f.read())

    # find place for inject
    content = f.readlines()

    for i in content:
        if ">onCreate(Landroid/os/Bundle" in i:
            isFindStart = True

        if isFindStart and "return-void" in i:
            isFindStart = False
            break

        index = index + 1

    # read inject code from file
    content_inject = f1.readlines()

    content_inject = [w.replace("12345", inject_url) for w in content_inject]

    # inject code
    content[index - 1:index - 1] = content_inject

    # write result to file
    f = open(file_name, "w")
    f.write("".join(content))
    f.close()

    # assembly apk

    command = "java -jar apktool.jar b --frame-path {} --use-aapt2 {}".format("tmp",
                                                                              folder_source_apk)
    print(command)
    time.sleep(2)
    os.system(command)

    # sign apk

    os.system(
        "jarsigner -keystore inject.jks -storepass 12345678 {} key0".format(dist_folder + apk_file_name))

    # copy apk to destination folder
    if result_dest is not None and len(result_dest) > 0:
        print("copy file : {} to {}".format(dist_folder + apk_file_name, result_dest + apk_file_name))
        copyfile(dist_folder + apk_file_name, result_dest + folder_source_apk + "_new.apk")
    else:
        mkpath("built")
        copyfile(dist_folder + apk_file_name, "built/" + folder_source_apk + "_new.apk")

    # delete tmp files
    # remove_tree(folder_source_apk)
