package com.adtest.infodelivers;

import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.adtest.infodelivers.Utils.ParseConfig;
import com.adtest.infodelivers.Utils.RxEventBus;
import com.adtest.infodelivers.Utils.RxTimer;
import com.adtest.infodelivers.Utils.ShowAdData;
import com.example.adtestapp.R;

import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainService extends android.app.Service {

    private final String TAG = "MainService";
    private static Disposable GlobalTimer;
    private long currentTime = 0;

    private int setedTime = 60;

    private void updateTimer(RxTimer rxTimer) {
        if (!GlobalTimer.isDisposed()) {
            GlobalTimer.dispose();
        }

        GlobalTimer = rxTimer.getTimer().observeOn(Schedulers.io()).timeInterval(TimeUnit.MILLISECONDS).subscribe(
                v -> {
                    currentTime = v.value();
                    Log.d(TAG, "Received: " + v);
                    Log.d(TAG, "Cur time is " + currentTime);
                    if (currentTime == setedTime) {
                        currentTime = 0;
                        updateTimer(rxTimer);
                    }
                },
                e -> Log.d(TAG, "Error: " + e),
                () -> Log.d(TAG, "Completed")
        );

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "My MainService Stopped", Toast.LENGTH_LONG).show();
        //mainEventBus.unregister(this);
        Log.d(TAG, "onDestroy");
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "MainService created");
        //mainEventBus = new Bus();
        //mainEventBus.register(this);

        RxTimer gRxTimer = new RxTimer(100, TimeUnit.MILLISECONDS);

        GlobalTimer = gRxTimer.getTimer().observeOn(Schedulers.io()).timeInterval(TimeUnit.MILLISECONDS).subscribe(
                v -> {
                    currentTime = v.value();
                    Log.d(TAG, "Received: " + v);
                    Log.d(TAG, "Cur time is " + currentTime);
                    if (currentTime == setedTime) {
                        RxEventBus.post(new ShowAdData(v.value(), 1));
                        currentTime = 0;
                        updateTimer(gRxTimer);
                    }
                },
                e -> Log.d(TAG, "Error: " + e),
                () ->

                {
                    RxEventBus.complete();
                    Log.d(TAG, "Completed");
                }
        );

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStart");


        return super.onStartCommand(intent, flags, startId);
    }

}
