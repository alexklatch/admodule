package com.adtest.infodelivers.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.adtest.infodelivers.AdMobServices.AdMobService;
import com.adtest.infodelivers.MainService;

public class onBootReceiver extends BroadcastReceiver {

    private final String TAG = "Autostart";

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, MainService.class);
        Intent i2 = new Intent(context, AdMobService.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(i);
            context.startForegroundService(i2);
        } else {
            context.startService(i);
            context.startService(i2);
        }
        Log.i(TAG, "Device started");
    }
}
