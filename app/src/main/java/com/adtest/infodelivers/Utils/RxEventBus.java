package com.adtest.infodelivers.Utils;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public final class RxEventBus {

    private static PublishSubject<Object> subject = PublishSubject.create();

    private RxEventBus() {
        subject.subscribeOn(Schedulers.io());
    }

    public static Disposable subscribe(@NonNull Consumer<Object> action) {
        return subject.subscribe(action);
    }

    public static void post(@NonNull Object message) {
        subject.onNext(message);
    }

    public static void complete() {
        subject.onComplete();

    }

}
