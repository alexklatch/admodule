package com.adtest.infodelivers.Utils;

import android.util.Log;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

public class RxTimer {

    private final String TAG = "RxTimer";
    private boolean reset = false;
    private int period;
    private TimeUnit timeUnit;

    public RxTimer(int period, TimeUnit timeUnit) {
        Log.d(TAG, "Preparing timers for init");
        this.period = period;
        this.timeUnit = timeUnit;
    }

    public Observable<Long> getTimer() {
        return Observable.interval(period, timeUnit);
    }


}
