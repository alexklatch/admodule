package com.adtest.infodelivers.Utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;

public class ParseConfig {

    private int path;
    private Context ctx;
    private Resources resources;
    private final String TAG = "ConfigParser";
    private HashMap<String, String> dictionary;
    private boolean isParsed = false;

    public ParseConfig(int path, Context ctx) {
        this.path = path;
        this.ctx = ctx;
        dictionary = new HashMap<>();
        resources = ctx.getResources();
    }

    public HashMap<String, String> parse() throws IOException, JSONException {
        InputStream config = resources.openRawResource(path);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(config, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } finally {
            config.close();
        }

        String jsonString = writer.toString();

        JSONObject wholeJsonObject = new JSONObject(jsonString);


        Log.d(TAG, "Parsing success!");

        dictionary.put("DownloadSite", (String) wholeJsonObject.getJSONObject("globSetting").get("downloadSite"));
        dictionary.put("AdSite", (String) wholeJsonObject.getJSONObject("adRes").get("site1"));
        dictionary.put("Freq", (String) wholeJsonObject.getJSONObject("timing").get("freq"));

        isParsed = true;

        return dictionary;
    }


    private HashMap<String, String> getParsed() {
        if (isParsed) {
            return dictionary;
        } else {
            Log.d(TAG, "Not parsed");
            return null;
        }
    }

}
