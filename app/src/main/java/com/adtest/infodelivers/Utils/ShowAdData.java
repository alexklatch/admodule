package com.adtest.infodelivers.Utils;

public class ShowAdData {

    private long a;
    private int b;

    public ShowAdData(long a, int b) {
        this.a = a;
        this.b = b;
    }

    public long getA() {
        return a;
    }

    public int getB() {
        return b;
    }
}
