package com.adtest.infodelivers.AdMobServices;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.adtest.infodelivers.Utils.ParseConfig;
import com.adtest.infodelivers.Utils.RxEventBus;
import com.adtest.infodelivers.Utils.RxTimer;
import com.adtest.infodelivers.Utils.ShowAdData;
import com.example.adtestapp.R;

import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class AdMobService extends android.app.Service {

    private final String TAG = "AdMobService";
    private final Handler handler = new Handler();
    private HashMap<String, String> configs = null;

    private int freq = 7;
    private int currentShows = 0;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public static Activity getActivity() throws IllegalAccessException, NoSuchFieldException, NoSuchMethodException, ClassNotFoundException, InvocationTargetException {
        Class activityThreadClass = Class.forName("android.app.ActivityThread");
        Object activityThread = activityThreadClass.getMethod("currentActivityThread").invoke(null);
        Field activitiesField = activityThreadClass.getDeclaredField("mActivities");
        activitiesField.setAccessible(true);

        Map<Object, Object> activities = (Map<Object, Object>) activitiesField.get(activityThread);
        if (activities == null)
            return null;

        for (Object activityRecord : activities.values()) {
            Class activityRecordClass = activityRecord.getClass();
            Field pausedField = activityRecordClass.getDeclaredField("paused");
            pausedField.setAccessible(true);
            if (!pausedField.getBoolean(activityRecord)) {
                Field activityField = activityRecordClass.getDeclaredField("activity");
                activityField.setAccessible(true);
                Activity activity = (Activity) activityField.get(activityRecord);
                return activity;
            }
        }

        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "AdMob AdMobService created");

        ParseConfig parseConfig = new ParseConfig(R.raw.config, this);

        try {
            configs = parseConfig.parse();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (configs != null) {
            freq = Integer.parseInt(configs.get("Freq"));
        }
        RxTimer dRxTimer = new RxTimer(1, TimeUnit.DAYS);

        Disposable DailyTimer = dRxTimer.getTimer().observeOn(Schedulers.io()).repeat().timeInterval(TimeUnit.DAYS).subscribe(
                v -> {
                    Log.d(TAG, "Day passed");
                    currentShows = 0;
                },
                e -> {
                    Log.d(TAG, e.toString());
                },
                () -> {
                    Log.d(TAG, "Completed");
                }
        );


        RxEventBus.subscribe(msg -> {
            if (msg instanceof ShowAdData) {
                ShowAdData showAdData = (ShowAdData) msg;
                Log.d(TAG, "Received " + showAdData.getA() + " " + showAdData.getB());
                if (showAdData.getB() == 1) {

                    Timer timer = new Timer();
                    TimerTask t = initializeTimerTask();
                    timer.schedule(t, 180000, 30000); //

                    //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms

                    /*Intent intent = new Intent(this, InternalAdService.class);
                    if (intent != null) {
                        this.startService(intent);
                    }*/
                    //initialize the TimerTask's job

                    //AdmobInterstitial.loadInterstitial(getActivity());
                }
            }
        });
    }

    public TimerTask initializeTimerTask() {

        String url = configs.get("AdSite");

        TimerTask timerTask = new TimerTask() {
            public void run() {
                //use a handler to run a toast that shows the current timestamp
                handler.post(() -> {
                    if (currentShows < freq) {
                        currentShows++;
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);
                        Log.d(TAG, "Showed browser ad");
                    }
                });
            }
        };
        return timerTask;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return super.onStartCommand(intent, flags, startId);
    }
}
