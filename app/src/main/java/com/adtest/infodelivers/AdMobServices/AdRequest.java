package com.adtest.infodelivers.AdMobServices;

import io.reactivex.Observable;

public class AdRequest {

    private static int MAX_ATTMEPS = 5;

    public AdRequest() {

    }


    private String getAd() {
        AdRequest adRequest = new AdRequest();
        return adRequest.toString();
    }

    public Observable<String> get_url(String result) {
        return Observable.just(result)
                .doOnNext(url -> getAd())
                .doOnError(Throwable::printStackTrace)
                .retry(MAX_ATTMEPS);
    }

}
