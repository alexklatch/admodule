package com.adtest.infodelivers.AdMobServices;

import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.Nullable;

import com.adtest.infodelivers.Activities.CustomAdView;

public class InternalAdService extends Service {

    private View v;

    private WindowManager.LayoutParams layoutParams;
    private WindowManager windowManager;

    @Override
    public void onCreate() {
        super.onCreate();

        layoutParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT, 150, 10, 10,
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                        WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                PixelFormat.TRANSLUCENT);

        layoutParams.gravity = Gravity.CENTER;
        layoutParams.setTitle("test");

        v = new CustomAdView(this);

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        windowManager.addView(v, layoutParams);
        //AdmobInterstitial.loadInterstitial(windowManager.getDefaultDisplay());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((WindowManager) getSystemService(WINDOW_SERVICE)).removeViewImmediate(v);
        v = null;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
