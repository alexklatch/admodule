package com.adtest.infodelivers.Activities;

import android.app.Activity;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class AdmobInterstitial {
    public static final String TAG = "Ground";
    public static final String ID = "ca-app-pub-3940256099942544/1033173712";
    private static ScheduledFuture loaderHandler;

    public static void loadInterstitial(final Activity activity) {
        final Runnable loader = new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Loading Admob interstitial...");
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final InterstitialAd interstitial = new InterstitialAd(activity);
                        interstitial.setAdUnitId(ID);
                        AdRequest adRequest = new AdRequest.Builder().build();
                        interstitial.loadAd(adRequest);
                        interstitial.setAdListener(new AdListener() {
                            public void onAdLoaded() {
                                displayInterstitial(interstitial);
                            }
                        });
                    }
                });
            }
        };

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        loaderHandler = scheduler.scheduleWithFixedDelay(loader, 0, 30, TimeUnit.SECONDS);
    }

    private static void displayInterstitial(final InterstitialAd interstitialAd) {
        if (interstitialAd.isLoaded()) {
            interstitialAd.show();
        }
    }
}